interface Component {
    name: string,
    component: Function,
    before: Function,
    after: Function
}

class Oneery {
    components: Array<Component>
    elems: Array<Document> | Array<Window> | NodeListOf<Element>;
    constructor(selector: Document | Window | string | void) {
        this.components = [];
        this.elems = [document];
        if (!selector) return;
		if (selector instanceof  Document) {
			this.elems = [document];
		} else if (selector instanceof Window) {
			this.elems = [window];
		} else {
			this.elems = document.querySelectorAll(selector);
		}
    }
    each(callback: Function): this | undefined {
        if (!callback || typeof callback !== 'function') return;
        for (var i = 0; i < this.elems.length; i++) {
            callback(this.elems[i], i);
        }
        return this;
    }
    add_class(className: string): this {
        this.each(function(item: Element): void {
            item.classList.add(className);
        });
        return this;
    }
    remove_class(className: string): this {
        this.each(function(item: Element): void {
            item.classList.remove(className);
        });
        return this;
    }
    append(element: Node): this {
        this.each(function(item: Element): void {
            item.appendChild(element);
        });
        return this;
    }
    after(element: Element): this {
        this.each(function (item: Element) {
            item.insertAdjacentElement('afterend', element);
        });
        return this;
    }
    before(element: Element): this {
        this.each(function (item: Element): void {
            item.insertAdjacentElement('beforebegin', element);
        })
        return this;
    }
    empty(): this {
        this.each(function (item: Element): void {
            while (item.firstChild) {
                item.removeChild(item.firstChild)
            }
        });
        return this;
    }
    remove(): this {
        this.each(function (item: Element): void {
           item.parentNode.removeChild(item); 
        });
        return this;
    }
    replace_with(replace: string): this {
        this.each(function (item: Element): void {
            item.outerHTML = replace;
        });
        return this;
    }
    html(html: string): this {
        this.each(function (item: Element): void {
            item.innerHTML = html;
        });
        return this;
    }
    add_html(html: string): this {
        this.each(function (item: Element): void {
            item.innerHTML += html;
        });
        return this;
    }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          EVENTS
    */ ////////////////////////////////////////////////////////////////////////////////
    on(event: string, callback: EventListenerOrEventListenerObject): this {
        this.each(function (item: Element): void {
           item.addEventListener(event, callback) ;
        });
        return this;
    }
    off(event: string, callback: Function): this {
        this.each(function (item: Element): void {
            item.removeEventListener(event, callback());
        });
        return this;
    }
    trigger(type: string, eventInitDict: CustomEventInit<any>): this {
        this.each(function (item: Element): void {
            const event: CustomEvent = new CustomEvent(type, eventInitDict);
            item.dispatchEvent(event);
        });
        return this;
    }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          EVENTS
    */ ////////////////////////////////////////////////////////////////////////////////
    /* ////////////////////////////////////////////////////////////////////////////////
                                          COMPONENTS
    */ ////////////////////////////////////////////////////////////////////////////////
    create_component(component: Component): this {
        this.components.push(component);
        return this;
    }
    delete_component(name: string): this {
        for (let i = 0; i < this.components.length; i++) {
            const component = this.components[i];
            if (name === component.name) {
                this.components.splice(i, 1);
                i--;
            }
        }
        return this;
    }
    draw_component(name: string): this {
        this.each((item: Element) => {
            this.components.forEach((component: Component) => {
                if(component.name === name) {
                    component.before();
                    item.innerHTML = component.component();
                    component.after();
                }
            });
        });
        return this;
    }
    draw_component_with_props(name: string, properties: object): this {
        this.each(function (item: Element): void {
            for (let i = 0; i < this.components.length; i++) {
                const component: Component = this.components[i];
                component.before();
                item.innerHTML = component.component(properties);
                component.after();
            }
        });
        return this;
    }
    add_component(name: string): this {
        this.each(function (item: Element): void {
            for (let i = 0; i < this.components.length; i++) {
                const component: Component = this.components[i];
            component.before();
            item.innerHTML += component.component();
            component.after();
            }
        });
        return this;
    }
    add_component_with_props(name: string, properties: object): this {
        this.each(function (item: Element): void {
            for (let i = 0; i < this.components.length; i++) {
                const component: Component = this.components[i];
                component.before();
                item.innerHTML += component.component(properties);
                component.after();
            }
        });
        return this;
    }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          COMPONENTS
    */ ////////////////////////////////////////////////////////////////////////////////
    uuidv4(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}

export default function o(selector: Document | Window | string | void): Oneery {
    return new Oneery(selector);
}