/**
 * Store class holds data.
 */
class Store {
    /**
     * Store constructor.
     * @param {any} data New data that the store will hold.
     */
    constructor(data){
        this.observers = [];
        this.data = data;
    }
    /**
     * Suscribes a observer object to its data.
     * @param {Observer} newObserver Is the observer object that will suscribe to the store.
     */
    suscribe(newObserver) {
        this.observers.push({
            id: newObserver.getId(),
            observer: newObserver
        });
    }
    /**
     * Unsuscribes an observer object from the store.
     * @param {Observer} Observer Observer object.
     */
    unsuscribe(Observer){
        for (let i = 0; i < this.observers.length; i++) {
            const observer = this.observers[i];
            if (observer.id ===  Observer.getId()) {
                this.observers.splice(i, 1);
                i--;
            }
        }
    }
    /**
     * Notifies the observers when data is updated and sends it.
     * @param {any} data Updated data.
     */
    notify(data) {
        this.observers.forEach(observer => {
            observer.observer.onChange(data);
        });
    }
    /**
     * Updates the store data.
     * @param {any} newData New data.
     */
    update(newData) {
        this.data = newData;
        this.notify(newData);
    }
}

/**
 * Class that suscribes to stores.
 */
class Observer {
    /**
     * Observer constructor.    
     * @param {(any) => void} callback A callback to call when data updates.
     */
    constructor(callback) {
        this.callback = callback;
        this.id = O().uuidv4();
    }
    /**
     * Returns observer unique id.
     * @returns {string} Id
     */
    getId() {
        return this.id;
    }
    /**
     * Function called by Store when data changes.
     * @param {any} Data Store data.
     */
    onChange(Data) {
        this.callback(Data);
    }
}

/**
 * Oneery lib main function.
 */
const O = (function () {
	/**
     * Oneery constructor.
     * @param {Document | Window | string | null} selector Element selector.
     * @returns Nothig.
     */
	const Constructor = function (selector) {
		if (!selector) return;
		if (selector === 'document') {
			this.elems = [document];
		} else if (selector === 'window') {
			this.elems = [window];
		} else {
			this.elems = document.querySelectorAll(selector);
		}
	};

    /**
     * Iterates over all elements and calls the callback.
     * @param {(Element) => void} callback Recives a element.
     * @returns this.
     */
	Constructor.prototype.each = function (callback) {
		if (!callback || typeof callback !== 'function') return;
		for (var i = 0; i < this.elems.length; i++) {
			callback(this.elems[i], i);
		}
		return this;
	};

    /* ////////////////////////////////////////////////////////////////////////////////
                                          ELEMENTS
    */ ////////////////////////////////////////////////////////////////////////////////
	/**
     * Adds a class to the element.
     * @param {string} className New class name.
     * @returns this.
     */
    Constructor.prototype.addClass = function (className) {
		this.each(function (item) {
			item.classList.add(className);
		});
		return this;
	};
    /**
     * Removes a class from the element.
     * @param {string} className Class name.
     * @returns this.
     */
	Constructor.prototype.removeClass = function (className) {
		this.each(function (item) {
			item.classList.remove(className);
		});
		return this;
	};
    /**
     * Appends a child to the element.
     * @param {Element} element New element to add.
     * @returns this.
     */
    Constructor.prototype.append = function (element) {
        this.each(function (item) {
            item.appendChild(element);
        });
        return this;
    }
    /**
     * Add a element after end to the element.
     * @param {Element} element New element to add.
     * @returns this.
     */
    Constructor.prototype.after = function (element) {
        this.each(function (item) {
            item.insertAdjacentElement('afterend', element);
        });
        return this;
    }
    /**
     * Add a element before begin to the element.
     * @param {Element} element New element to add.
     * @returns this.
     */
    Constructor.prototype.before = function (element) {
        this.each(function (item) {
            item.insertAdjacentElement('beforebegin', element);
        });
        return this;
    }
    /**
     * Clears the element.
     * @returns this.
     */
    Constructor.prototype.empty = function () {
        this.each(function (item) {
            while (item.firstChild) {
                item.removeChild(item.firstChild);
            }
        });
        return this;
    }
    /**
     * Gets the selecter attribute.
     * @param {Attr} attribute Attribute to get.
     * @returns this.
     */
    Constructor.prototype.getAttr = function (attribute) {
        let attrs = [];
        this.each(function (item) {
            attrs.push(item.getAttribute(attribute))
        });
        return attrs;
    }
    /**
     * Gets element text.
     * @returns Text.
     */
    Constructor.prototype.getText = function () {
        let text = []; 
        this.each(function (item) {
            text.push(item.textContent);
        });
        return text;
    }
    /**
     * Returns array of parent nodes.
     * @returns node array.
     */
    Constructor.prototype.getParentNode = function () {
        let nodes = [];
        this.each(function (item) {
            nodes.push(item.parentNode);
        });
        return nodes;
    }
    /**
     * Returns array of previous elements.
     * @returns Element.
     */
    Constructor.prototype.getPrev = function () {
        let elements = [];
        this.each(function (item) {
            elements.push(item.previousElementSibling);
        });
        return elements;
    }
    /**
     * Removes actual element.
     * @returns this.
     */
    Constructor.prototype.remove = function () {
        this.each(function (item) {
            item.parentNode.removeChild(item);
        });
        return this;
    }
    /**
     * Removes the selecter attr from the element.
     * @param {Attr} attr Attr to find.
     * @returns this.
     */
    Constructor.prototype.removeAttr = function (attr) {
        this.each(function (item) {
            item.removeAttribute(attr);
        });
        return this;
    }
    /**
     * Replaces outer html.
     * @param {string} replacer HTML text replacer.
     * @returns this.
     */
    Constructor.prototype.replaceWith = function (replacer) {
        this.each(function (item) {
            item.outerHTML = replacer;
        });
        return this;
    }
    /**
     * Sets Attribute to the element.
     * @param {Attr} attr Attribute.
     * @param {any} value Value.
     * @returns this.
     */
    Constructor.prototype.setAttr = function (attr, value) {
        this.each(function (item) {
            item.setAttribute(attr, value);
        });
        return this;
    }
    /**
     * Sets innerHTML to the element.
     * @param {string} html HTML text.
     * @returns this.
     */
    Constructor.prototype.html = function (html) {
        this.each(function (item) {
            item.innerHTML = html;
        });
        return this;
    }
    /**
     * Sets text content of the element.
     * @param {string} text Text.
     * @returns this.
     */
    Constructor.prototype.text = function (text) {
        this.each(function (item) {
            item.textContent = text;
        });
        return this;
    }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          ELEMENTS
    */ ////////////////////////////////////////////////////////////////////////////////

    /* ////////////////////////////////////////////////////////////////////////////////
                                          EVENTS
    */ ////////////////////////////////////////////////////////////////////////////////
    /**
     * Adds on event to the element.
     * @param {Event} event Trigger event.
     * @param {EventListener} eventHandler Callback.
     * @returns this.
     */
    Constructor.prototype.on = function (event, eventHandler) {
        this.each(function (item) {
            item.addEventListener(event, eventHandler);
        });
        return this;
    }
    /**
     * Adds of event to the element.
     * @param {Event} event Trigger event.
     * @param {EventListener} eventHandler Callback.
     * @returns this.
     */
    Constructor.prototype.off = function (event, eventHandler) {
        this.each(function (item) {
            item.removeEventListener(event, eventHandler);
        });
        return this;
    }
    /**
     * Add trigger to the element.
     * @param {string} eventName Name of the event.
     * @param {Object} data data.
     * @returns this.
     */
    Constructor.prototype.trigger = function (eventName, data) {
        this.each(function (item) {
            const event = new CustomEvent(eventName, {detail: data});
            item.dispatchEvent(event);
        });
        return this
    }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          EVENTS
    */ ////////////////////////////////////////////////////////////////////////////////
    
    /* ////////////////////////////////////////////////////////////////////////////////
                                          UTILS
    */ ////////////////////////////////////////////////////////////////////////////////
    /**
     * Parsers html.
     * @param {string} content html content to add.
     * @returns Body children.
     */
    Constructor.prototype.parseHtml = function (content) {
        const tmp = document.implementation.createHTMLDocument();
        tmp.body.innerHTML = content;
        return tmp.body.children;
    }
    /**
     * Creates uuidv4.
     * @returns Id uuidv4 string.
     */
    Constructor.prototype.uuidv4 =  function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
      }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          UTILS
    */ ////////////////////////////////////////////////////////////////////////////////
    
    /* ////////////////////////////////////////////////////////////////////////////////
                                          STORE
    */ ////////////////////////////////////////////////////////////////////////////////
    /**
     * Creates a store.
     * @param {any} newData Data to add. 
     * @returns Store.
     */
    Constructor.prototype.store = function (newData) {
        return new Store(newData);
    }
    /**
     * Creates a Observer.
     * @param {(any) => void} callback Callback when data changes.
     * @returns Observer.
     */
    Constructor.prototype.observer = function (callback) {
        return new Observer(callback);
    }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          STORE
    */ ////////////////////////////////////////////////////////////////////////////////

    /* ////////////////////////////////////////////////////////////////////////////////
                                          FETCH
    */ ////////////////////////////////////////////////////////////////////////////////
    /**
     * Fetch post.
     * @param {string} url Url.
     * @param {any} data Data to post.
     * @returns Fetch promise.
     */
    Constructor.prototype.post = function (url, data) {
        return fetch(url, {
            method: "POST",
            body: JSON.stringify(data)
        });
    }
    /**
     * Fetch get.
     * @param {string} url Url.
     * @returns Fetch promise.
     */
    Constructor.prototype.get = function (url) {
        return fetch(url)
            .then(r => r.JSON())
            .then(r => r)
            .catch(e => console.error(e))
    }
    /* ////////////////////////////////////////////////////////////////////////////////
                                          FETCH
    */ ////////////////////////////////////////////////////////////////////////////////
    /**
     * Creates a constructor object.
     * @param {Document | Window | string} selector Element selector
     * @returns Constructor.
     */
	const instantiate = function (selector = null) {
		return new Constructor(selector);
	};

	return instantiate;

})();
